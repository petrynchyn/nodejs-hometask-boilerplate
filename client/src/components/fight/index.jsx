import React from 'react';

import { getFighters } from '../../services/domainRequest/fightersRequest';
import { getFights, createFight } from '../../services/domainRequest/fightRequest';
import NewFighter from '../newFighter';
import Fighter from '../fighter';
import FightHistory from '../fightHistory';
import { Button, Snackbar } from '@material-ui/core';

import './fight.css'

class Fight extends React.Component {
  state = {
    fighters: [],
    fighter1: null,
    fighter2: null,
    winner: null,
    isFighting: false,
    isBtnDisabled: true,
    logFights: [],
  };

  async componentDidUpdate(prevProps, prevState) {
    const { fighter1, fighter2, isFighting } = this.state;
    if ( fighter1 && fighter2 && ( fighter1 !== prevState.fighter1 || fighter2 !== prevState.fighter2 )
    ) {
      const logFights = await getFights( fighter1.id, fighter2.id );
      if (logFights && !logFights.error) {
        this.setState({ logFights });
      }
      if (!isFighting) this.setState({ isBtnDisabled: false });
    }
  }

  async componentDidMount() {
    const fighters = await getFighters();
    if (fighters && !fighters.error) {
      this.setState({ fighters });
    }
  }

  onFightStart = (e) => {
    // if (!this.state.fighter1 || !this.state.fighter2) return;

    this.setState({ isBtnDisabled: true, isFighting: true, winner: null });

    const attack1F = 'KeyA',
          block1F = 'KeyD',
          attack2F = 'KeyJ',
          block2F = 'KeyL',
          crHitComb1F = ['KeyQ', 'KeyW', 'KeyE'],
          crHitComb2F = ['KeyU', 'KeyI', 'KeyO'];

    let logFight = {
          fighter1: this.state.fighter1.id,
          fighter2: this.state.fighter2.id,
          log: [],
        };

    let isBlocked1F = false,
        isBlocked2F = false;

    let isAllowedCrHit1F = true,
        isPressedKey1CrHit1F = false,
        isPressedKey2CrHit1F = false,
        isPressedKey3CrHit1F = false;

    let isAllowedCrHit2F = true,
        isPressedKey1CrHit2F = false,
        isPressedKey2CrHit2F = false,
        isPressedKey3CrHit2F = false;

    
    let healthDamage = async (fighterKeyStr, isCriticalHit = false) => {
      
      let attacker, defender, damage;
      const { fighter1, fighter2, logFights } = this.state;

      if (fighterKeyStr === 'fighter1') {
        attacker = fighter2;
        defender = fighter1;
      } else {
        attacker = fighter1;
        defender = fighter2;
      }

      if (isCriticalHit) {
        damage = 2 * attacker.power;
      } else {
        damage = Math.max(0, Math.round( attacker.power * (1 + Math.random()) - defender.defense * (1 + Math.random())) );
      }

      this.setState({
        [fighterKeyStr]: { ...defender, health: Math.max(0, defender.health - damage) },
      });

      logFight.log.push({
        [fighterKeyStr === 'fighter1' ? 'fighter2Hit' : 'fighter1Hit']: damage,
        fighter1Health: this.state.fighter1.health,
        fighter2Health: this.state.fighter2.health,
      });

      if (!this.state[fighterKeyStr].health) {
        removeKeyEventListaners();

        this.setState({ isFighting: false });
        this.setState({ winner: `Fighter ${attacker.name.toUpperCase()} won!` });

        const fightNew = await createFight(logFight);
        if (fightNew && !fightNew.error) {
          logFight = fightNew;
        }
        this.setState({ logFights: [logFight, ...logFights] });
      }
    };

    let removeKeyEventListaners = () => {
      document.removeEventListener('keydown', handlerKeyDown);
      document.removeEventListener('keyup', handlerKeyUp);
    };

    let handlerKeyDown = (event) => {
      if (event.repeat) return;

      switch (event.code) {
        case block1F:
          isBlocked1F = true;
          break;

        case block2F:
          isBlocked2F = true;
          break;

        // PlayerOneCriticalHitCombination

        case crHitComb1F[0]:
          isPressedKey1CrHit1F = true;
          break;

        case crHitComb1F[1]:
          isPressedKey2CrHit1F = true;
          break;

        case crHitComb1F[2]:
          isPressedKey3CrHit1F = true;
          break;

        // PlayerTwoCriticalHitCombination

        case crHitComb2F[0]:
          isPressedKey1CrHit2F = true;
          break;

        case crHitComb2F[1]:
          isPressedKey2CrHit2F = true;
          break;

        case crHitComb2F[2]:
          isPressedKey3CrHit2F = true;
          break;
      }

      if (!isBlocked1F & !isBlocked2F) {
        if (event.code === attack1F) {
          healthDamage('fighter2');
        } else if (event.code === attack2F) {
          healthDamage('fighter1');
        }
      }

      if ( isAllowedCrHit1F & !isBlocked1F & isPressedKey1CrHit1F & isPressedKey2CrHit1F & isPressedKey3CrHit1F ) {
        isAllowedCrHit1F = false;
        setTimeout(() => { isAllowedCrHit1F = true }, 10000);
        healthDamage('fighter2', true);
      }

      if ( isAllowedCrHit2F & !isBlocked2F & isPressedKey1CrHit2F & isPressedKey2CrHit2F & isPressedKey3CrHit2F ) {
        isAllowedCrHit2F = false;
        setTimeout(() => { isAllowedCrHit2F = true }, 10000);
        healthDamage('fighter1', true);
      }
    };

    let handlerKeyUp = (event) => {
      switch (event.code) {
        case block1F:
          isBlocked1F = false;
          break;

        case block2F:
          isBlocked2F = false;
          break;

        // PlayerOneCriticalHitCombination

        case crHitComb1F[0]:
          isPressedKey1CrHit1F = false;
          break;

        case crHitComb1F[1]:
          isPressedKey2CrHit1F = false;
          break;

        case crHitComb1F[2]:
          isPressedKey3CrHit1F = false;
          break;

        // PlayerTwoCriticalHitCombination

        case crHitComb2F[0]:
          isPressedKey1CrHit2F = false;
          break;

        case crHitComb2F[1]:
          isPressedKey2CrHit2F = false;
          break;

        case crHitComb2F[2]:
          isPressedKey3CrHit2F = false;
          break;
      }
    };

    document.addEventListener('keydown', handlerKeyDown);
    document.addEventListener('keyup', handlerKeyUp);
  }

  onCreate = (fighter) => {
    this.setState({ fighters: [...this.state.fighters, fighter] });
  }

  onFighter1Select = (fighter1) => {
    this.setState({ fighter1 });
  }

  onFighter2Select = (fighter2) => {
    this.setState({ fighter2 });
  }

  getFighter1List = () => {
    const { fighter2, fighters } = this.state;
    if (!fighter2) {
      return fighters;
    }

    return fighters.filter(it => it.id !== fighter2.id);
  }

  getFighter2List = () => {
    const { fighter1, fighters } = this.state;
    if (!fighter1) {
      return fighters;
    }

    return fighters.filter(it => it.id !== fighter1.id);
  }

  render() {
    const { fighter1, fighter2, winner, isFighting, logFights } = this.state;

    return (
      <div id="wrapper">
        <NewFighter onCreated={this.onCreate} />
        <div id="figh-wrapper">
          <Fighter selectedFighter={fighter1} onFighterSelect={this.onFighter1Select} fightersList={this.getFighter1List() || []} isFighting={isFighting} />
          <div className="btn-wrapper">
            <Button onClick={this.onFightStart} variant="contained" color="primary" disabled={this.state.isBtnDisabled}>Start Fight</Button>
          </div>
          <Fighter selectedFighter={fighter2} onFighterSelect={this.onFighter2Select} fightersList={this.getFighter2List() || []} isFighting={isFighting} />
        </div>
        <Snackbar anchorOrigin={{ vertical: 'top', horizontal: 'center' }} open={!!winner} message={winner} />
        <div id="fighs-history-wrapper">
          <FightHistory logFights={logFights} name1F={fighter1?.name} name2F={fighter2?.name} />
        </div>
      </div>
    );
  }
}

export default Fight;