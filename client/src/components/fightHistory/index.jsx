import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import Box from '@material-ui/core/Box';
import Collapse from '@material-ui/core/Collapse';
import IconButton from '@material-ui/core/IconButton';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';

const useRowStyles = makeStyles({
  root: {
    '& > *': {
      borderBottom: 'unset',
    },
  },
});


function Row(props) {
  const { row, name1F, name2F } = props;
  const [open, setOpen] = React.useState(false);
  const classes = useRowStyles();

  return (
    <React.Fragment>
      <TableRow className={classes.root} hover={true} >
        <TableCell align="center"  size="small" padding="none">
          <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton> 
        </TableCell>
        <TableCell component="th" scope="row" >
          {new Date(row.createdAt).toLocaleString() || 'Last Fight'}
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={2}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <Box margin={1}>
              <Typography variant="h6" gutterBottom component="div">
                Fight History
              </Typography>
              <Table size="small" aria-label="purchases">
                <TableHead>
                  <TableRow>
                    <TableCell align="center" size="small" padding="none">#</TableCell>
                    <TableCell align="right">{name1F} Hit</TableCell>
                    <TableCell align="right">{name2F} Hit</TableCell>
                    <TableCell align="right">{name1F} Health</TableCell>
                    <TableCell align="right">{name2F} Health</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {row.log.map((logRow, index) => (
                    <TableRow>
                      <TableCell align="center" size="small" padding="none" component="th" scope="row">
                        <small><i>{++index}</i></small>
                      </TableCell>
                      <TableCell align="right">{logRow.fighter1Hit}</TableCell>
                      <TableCell align="right">{logRow.fighter2Hit}</TableCell>
                      <TableCell align="right">{logRow.fighter1Health}</TableCell>
                      <TableCell align="right">{logRow.fighter2Health}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
}

Row.propTypes = {
  row: PropTypes.shape({
    id: PropTypes.string,
    createdAt: PropTypes.string,
    log: PropTypes.arrayOf(
      PropTypes.shape({
        fighter1Hit: PropTypes.number,
        fighter2Hit: PropTypes.number,
        fighter1Health: PropTypes.number.isRequired,
        fighter2Health: PropTypes.number.isRequired,
      }),
    ).isRequired,
  }).isRequired,
};


export default function FightHistory({logFights, name1F = 'Fighter 1 ', name2F = 'Fighter 2'}) {
  return (
    <TableContainer component={Paper}>
      <Table aria-label="collapsible table">
        <TableHead>
          <TableRow>
            <TableCell align="right"  size="small" padding="checkbox">Fights</TableCell>
            <TableCell align="center">
              <b>{name1F.toUpperCase()}</b> vs <b>{name2F.toUpperCase()}</b>
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {logFights.map((row) => (
            <Row key={row.id} row={row} name1F={name1F} name2F={name2F} />
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
}