import { get, post } from "../requestHelper";

const entity = 'fights';

export const getFights = async (idF1, idF2) => {
    return await get(`${entity}/${idF1}`, idF2);
}

export const createFight = async (body) => {
    return await post(entity, body);
}