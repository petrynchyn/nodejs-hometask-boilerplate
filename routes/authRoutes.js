const { Router } = require('express');
const AuthService = require('../services/authService');

const router = Router();

router.post('/login', (req, res, next) => {
        // TODO: Implement login action (get the user if it exist with entered credentials)
    
    const data = AuthService.login({ email: req.body.email })
    if (data.password != req.body.password) {
        throw Error('Wrong password! ');
    }
    
    res.data = data;

    next();

});

router.post('/', (req, res, next) => {
        // TODO: Implement login action (get the user if it exist with entered credentials)

    const data = AuthService.loginChat({ login: req.body.login })
    if (data.password != req.body.password) {
        throw Error('Wrong password! ');
    }
    
    res.data = data;

    next();

});

module.exports = router;