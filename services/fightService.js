const { FightRepository } = require('../repositories/fightRepository');
class FightService {
    // OPTIONAL TODO: Implement methods to work with fights

    getFights(search) {

        const item = FightRepository.filterSortReverse(search, ['createdAt'])
        if (!item) {
            throw Error('Get fights failed. ')
        }
        return item;
    }
    
    create(data) {

        const item = FightRepository.create(data)
        if(!item) {
            throw Error('Add fight failed. ')
        }
        return item;
    }
}

module.exports = new FightService();