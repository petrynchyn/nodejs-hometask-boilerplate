const { FighterRepository } = require('../repositories/fighterRepository');

class FighterService {
    // TODO: Implement methods to work with fighters

    search(search) {

        const item = FighterRepository.getOne(search);
        if(!item) {
            throw Error('Fighter not found. ')
        }
        return item;
    }

    getAll() {
        return FighterRepository.getAll();
    }

    create(data) {

        let item = FighterRepository.getOne({name: data.name});
        if(item) {
            throw Error(`Fighter ${data.name} already exists. `)
        }

        item = FighterRepository.create(data);
        if(!item) {
            throw Error('Add fighter failed. ')
        }
        return item;
    }

    update(id, data) {

        let item = FighterRepository.update(id, data);
        if(!item) {
            throw Error('Update fighter failed, fighter not found. ')
        }
        return item;
    }
    
    delete(id) {

        let item =  FighterRepository.delete(id);
        if (!item.length) {
            throw Error('Delete fighter failed, fighter not found. ')
        }
        return item;
    }

}

module.exports = new FighterService();