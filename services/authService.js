const UserService = require('./userService');
const UserChatService = require('./userChatService');

class AuthService {
    login(userData) {
        const user = UserService.search(userData);
        if(!user) {
            throw Error('User not found');
        }
        return user;
    }

    loginChat(userData) {
        const user = UserChatService.search(userData);
        if(!user) {
            throw Error('User not found');
        }
        return user;
    }
}

module.exports = new AuthService();