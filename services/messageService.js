const { MessageRepository } = require('../repositories/messageRepository');

class MessageService {
    // TODO: Implement methods to work with messages

    search(search) {

        const item = MessageRepository.getOne(search);
        if(!item) {
            throw Error('Message not found. ')
        }
        return item;
    }

    getAll() {
        return MessageRepository.getAll();
    }

    create(data) {

        // let item = MessageRepository.getOne({name: data.name});
        // if(item) {
        //     throw Error(`Message ${data.name} already exists. `)
        // }

        const item = MessageRepository.create(data);
        if(!item) {
            throw Error('Add message failed. ')
        }
        return item;
    }

    update(id, data) {

        let item = MessageRepository.update(id, data);
        if(!item) {
            throw Error('Update message failed, message not found. ')
        }
        return item;
    }
    
    delete(id) {

        let item =  MessageRepository.delete(id);
        if (!item.length) {
            throw Error('Delete message failed, message not found. ')
        }
        return item;
    }

}

module.exports = new MessageService();